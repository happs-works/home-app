/**
 * View Models used by Spring MVC REST controllers.
 */
package com.happs.homeowner.web.rest.vm;
